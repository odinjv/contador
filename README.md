# Contador


## Installation
Contador is configured to be developed on a Docker container to allow for better
compatibility between development environments. To install Contador you need to
have Docker installed.

To download and configure all the basic requirements for Contador run
`docker-compose build` on the root directory.

## Running the application
To run Contador on your development environment run `docker-compose up`. This
will run your project on port `8000` on your Docker machine.

## Running Django commands
If you need to run a Django command during Contador development use the
following syntax: `docker-compose run django django-admin <your command>`.

**Warning**: If you create any files with a Django command on a Linux machine,
the created files will be owned by root. Is recommended that you run
`sudo chown -R $USER:$USER src` to change ownership of those files.
